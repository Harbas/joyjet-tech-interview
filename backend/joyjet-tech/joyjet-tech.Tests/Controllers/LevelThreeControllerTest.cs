﻿using joyjet_tech.Controllers;
using joyjet_tech.Models;
using joyjet_tech.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace joyjet_tech.Tests.Controllers
{
    [TestClass]
    public class LevelThreeControllerTest
    {
        [TestMethod]
        public void Post()
        {
            // Arrange
            LevelThreeController controller = new LevelThreeController();

            #region Input
            var input = new InputLevelThreeViewModel()
            {
                articles = new List<Article>()
                {
                    new Article() { id = 1, name = "water", price = 100 },
                    new Article() { id = 2, name = "honey", price = 200 },
                    new Article() { id = 3, name = "mango", price = 400 },
                    new Article() { id = 4, name = "tea", price = 1000 },
                    new Article() { id = 5, name = "ketchup", price = 999 },
                    new Article() { id = 6, name = "mayonnaise", price = 999 },
                    new Article() { id = 7, name = "fries", price = 378 },
                    new Article() { id = 8, name = "ham", price = 147 },
                },
                carts = new List<Cart>()
                {
                    new Cart()
                    {
                        id = 1,
                        items = new List<Item>()
                        {
                            new Item() { article_id = 1, quantity = 6},
                            new Item() { article_id = 2, quantity = 2},
                            new Item() { article_id = 4, quantity = 1}
                        }
                    },
                    new Cart()
                    {
                        id = 2,
                        items = new List<Item>()
                        {
                            new Item() { article_id = 2, quantity = 1},
                            new Item() { article_id = 3, quantity = 3}
                        }
                    },
                    new Cart()
                    {
                        id = 3,
                        items = new List<Item>()
                        {
                            new Item() { article_id = 5, quantity = 1},
                            new Item() { article_id = 6, quantity = 1}
                        }
                    },
                    new Cart()
                    {
                        id = 4,
                        items = new List<Item>()
                        {
                            new Item() { article_id = 7, quantity = 1}
                        }
                    },
                    new Cart()
                    {
                        id = 5,
                        items = new List<Item>()
                        {
                            new Item() { article_id = 8, quantity = 3}
                        }
                    }
                },
                delivery_fees = new List<DeliveryFees>()
                {
                    new DeliveryFees()
                    {
                        price = 800,
                        eligible_transaction_volume = new EligibleTransactionVolume() { max_price = 1000, min_price = 0 }
                    },
                    new DeliveryFees()
                    {
                        price = 400,
                        eligible_transaction_volume = new EligibleTransactionVolume() { max_price = 2000, min_price = 1000 }
                    },
                    new DeliveryFees()
                    {
                        price = 0,
                        eligible_transaction_volume = new EligibleTransactionVolume() { max_price = null, min_price = 2000 }
                    }
                },
                discounts = new List<Discount>()
                {
                    new Discount() { article_id = 2, type = "amount", value = 25 },
                    new Discount() { article_id = 5, type = "percentage", value = 30 },
                    new Discount() { article_id = 6, type = "percentage", value = 30 },
                    new Discount() { article_id = 7, type = "percentage", value = 25 },
                    new Discount() { article_id = 8, type = "percentage", value = 10 }
                }
            };
            #endregion

            // Act
            OutputViewModel result = controller.Post(input) as OutputViewModel;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(5, result.carts.Count);

            // [0]
            Assert.AreEqual(1, result.carts[0].id);
            Assert.AreEqual(2350, result.carts[0].total);

            // [1]
            Assert.AreEqual(2, result.carts[1].id);
            Assert.AreEqual(1775, result.carts[1].total);

            // [2]
            Assert.AreEqual(3, result.carts[2].id);
            Assert.AreEqual(1798, result.carts[2].total);

            // [3]
            Assert.AreEqual(4, result.carts[3].id);
            Assert.AreEqual(1083, result.carts[3].total);

            // [4]
            Assert.AreEqual(5, result.carts[4].id);
            Assert.AreEqual(1196, result.carts[4].total);
        }
    }
}
