﻿using joyjet_tech.Controllers;
using joyjet_tech.Models;
using joyjet_tech.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace joyjet_tech.Tests.Controllers
{
    [TestClass]
    public class LevelTwoControllerTest
    {
        [TestMethod]
        public void Post()
        {
            // Arrange
            LevelTwoController controller = new LevelTwoController();

            #region Input
            var input = new InputLevelTwoViewModel()
            {
                articles = new List<Article>()
                {
                    new Article() { id = 1, name = "water", price = 100 },
                    new Article() { id = 2, name = "honey", price = 200 },
                    new Article() { id = 3, name = "mango", price = 400 },
                    new Article() { id = 4, name = "tea", price = 1000 }
                },
                carts = new List<Cart>()
                {
                    new Cart()
                    {
                        id = 1,
                        items = new List<Item>()
                        {
                            new Item() { article_id = 1, quantity = 6},
                            new Item() { article_id = 2, quantity = 2},
                            new Item() { article_id = 4, quantity = 1}
                        }
                    },
                    new Cart()
                    {
                        id = 2,
                        items = new List<Item>()
                        {
                            new Item() { article_id = 2, quantity = 1},
                            new Item() { article_id = 3, quantity = 3}
                        }
                    },
                    new Cart()
                    {
                        id = 3,
                        items = new List<Item>() { }
                    }
                },
                delivery_fees = new List<DeliveryFees>()
                {
                    new DeliveryFees()
                    {
                        price = 800,
                        eligible_transaction_volume = new EligibleTransactionVolume() { max_price = 1000, min_price = 0 }
                    },
                    new DeliveryFees()
                    {
                        price = 400,
                        eligible_transaction_volume = new EligibleTransactionVolume() { max_price = 2000, min_price = 1000 }
                    },
                    new DeliveryFees()
                    {
                        price = 0,
                        eligible_transaction_volume = new EligibleTransactionVolume() { max_price = null, min_price = 2000 }
                    }
                }
            };
            #endregion

            // Act
            OutputViewModel result = controller.Post(input) as OutputViewModel;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.carts.Count);

            // [0]
            Assert.AreEqual(1, result.carts[0].id);
            Assert.AreEqual(2000, result.carts[0].total);

            // [1]
            Assert.AreEqual(2, result.carts[1].id);
            Assert.AreEqual(1800, result.carts[1].total);

            // [2]
            Assert.AreEqual(3, result.carts[2].id);
            Assert.AreEqual(800, result.carts[2].total);
        }
    }
}
