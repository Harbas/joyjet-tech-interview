﻿using joyjet_tech.Models;
using joyjet_tech.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace joyjet_tech.Helpers
{
    public static class Calc
    {
        public static OutputViewModel Carts(InputLevelOneViewModel vm)
        {
            var output = new OutputViewModel() { carts = new List<TotalCartViewModel>() };

            foreach (var item in vm.carts)
                output.carts.Add(TotalCart(vm.articles, item));

            return output;
        }

        public static OutputViewModel Carts(InputLevelTwoViewModel vm)
        {
            var output = new OutputViewModel() { carts = new List<TotalCartViewModel>() };

            foreach (var item in vm.carts)
                output.carts.Add(TotalCartWithShipping(vm.articles, item, vm.delivery_fees));

            return output;
        }

        public static OutputViewModel Carts(InputLevelThreeViewModel vm)
        {
            var output = new OutputViewModel() { carts = new List<TotalCartViewModel>() };

            foreach (var item in vm.carts)
                output.carts.Add(TotalCartWithDiscount(vm.articles, item, vm.delivery_fees, vm.discounts));

            return output;
        }

        public static TotalCartViewModel TotalCart(List<Article> articles, Cart cart)
        {
            TotalCartViewModel vm = new TotalCartViewModel() { id = cart.id };

            decimal total = 0;

            foreach (var item in cart.items)
            {
                Article article = articles.Where(a => a.id == item.article_id).SingleOrDefault();
                total += article.price * item.quantity;
            }

            vm.total = Math.Floor(total);
            
            return vm;
        }

        public static TotalCartViewModel TotalCartWithShipping(List<Article> articles, Cart cart, List<DeliveryFees> deliveryFees)
        {
            TotalCartViewModel vm = TotalCart(articles, cart);

            int shippingValue = FindDeliveryFees(vm.total, deliveryFees).price;
            vm.total += shippingValue;

            vm.total = Math.Floor(vm.total);

            return vm;
        }

        public static TotalCartViewModel TotalCartWithDiscount(List<Article> articles, Cart cart, List<DeliveryFees> deliveryFees, List<Discount> discounts)
        {
            TotalCartViewModel vm = TotalCart(articles, cart);

            decimal discount = GetDiscount(cart, discounts, articles);
            vm.total -= discount;

            int shippingValue = FindDeliveryFees(vm.total, deliveryFees).price;
            vm.total += shippingValue;

            vm.total = Math.Floor(vm.total);

            return vm;
        }

        public static DeliveryFees FindDeliveryFees(decimal total, List<DeliveryFees> deliveryFees)
        {
            DeliveryFees result = null;

            foreach (var item in deliveryFees)
            {
                if (total >= item.eligible_transaction_volume.min_price &&
                    (item.eligible_transaction_volume.max_price == null || total < item.eligible_transaction_volume.max_price))
                {
                    result = item;
                    break;
                }
            }

            return result;
        }

        public static decimal GetDiscount(Cart cart, List<Discount> discounts, List<Article> articles)
        {
            decimal result = 0;

            foreach (var item in cart.items)
            {
                Discount discount = discounts.Where(d => d.article_id == item.article_id).SingleOrDefault();

                if (discount != null)
                {
                    Article article = articles.Where(a => a.id == item.article_id).SingleOrDefault();
                    result += CalculateDiscount(discount, item, article);
                }
            }

            return result;
        }

        public static decimal CalculateDiscount(Discount discount, Item item, Article article)
        {
            decimal total = 0;

            decimal unitPrice = article.price;

            if (discount.type == "percentage")
                unitPrice -= (unitPrice * discount.value) / 100;
            else if (discount.type == "amount")
                unitPrice -= discount.value;

            total = (article.price - unitPrice) * item.quantity;

            return total;
        }
    }
}