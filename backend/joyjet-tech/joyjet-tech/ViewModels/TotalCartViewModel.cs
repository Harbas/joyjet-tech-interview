﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace joyjet_tech.ViewModels
{
    public class TotalCartViewModel
    {
        public int id { get; set; }

        public decimal total { get; set; }
    }
}