﻿using joyjet_tech.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace joyjet_tech.ViewModels
{
    public class InputLevelOneViewModel
    {
        public List<Article> articles { get; set; }

        public List<Cart> carts { get; set; }
    }
}