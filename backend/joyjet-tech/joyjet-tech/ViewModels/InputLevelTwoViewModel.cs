﻿using joyjet_tech.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace joyjet_tech.ViewModels
{
    public class InputLevelTwoViewModel : InputLevelOneViewModel
    {
        public List<DeliveryFees> delivery_fees { get; set; }
    }
}