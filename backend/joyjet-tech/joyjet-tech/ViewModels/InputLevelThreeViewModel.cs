﻿using joyjet_tech.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace joyjet_tech.ViewModels
{
    public class InputLevelThreeViewModel : InputLevelTwoViewModel
    {
        public List<Discount> discounts { get; set; }
    }
}