﻿using joyjet_tech.Helpers;
using joyjet_tech.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace joyjet_tech.Controllers
{
    public class LevelTwoController : ApiController
    {
        [HttpPost]
        public OutputViewModel Post(InputLevelTwoViewModel vm)
        {
            var carts = Calc.Carts(vm);

            return carts;
        }
    }
}
