﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace joyjet_tech.Models
{
    public class DeliveryFees
    {
        public EligibleTransactionVolume eligible_transaction_volume { get; set; }
        public int price { get; set; }
    }
}