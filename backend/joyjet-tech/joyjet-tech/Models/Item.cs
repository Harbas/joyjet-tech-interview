﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace joyjet_tech.Models
{
    public class Item
    {
        public int article_id { get; set; }
        public int quantity { get; set; }
    }
}