﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace joyjet_tech.Models
{
    public class Discount
    {
        public int article_id { get; set; }
        public string type { get; set; }
        public int value { get; set; }
    }
}